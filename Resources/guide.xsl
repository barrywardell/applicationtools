<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <title><xsl:value-of select="guide/title"/></title>
        <link href="../../../guide.css" rel="stylesheet" type="text/css"/>
      </head>
      <body>
        <h1><xsl:value-of select="guide/title"/></h1>

        <p class="description"><xsl:apply-templates select="guide/description"/></p>

        <xsl:apply-templates select="guide/packages"/>

        <!-- <h1><xsl:value-of select="guide/title"/></h1> -->

        
        <!-- <xsl:for-each select="guide/packages"> -->
        <!--   <h2><xsl:value-of select="title"/></h2> -->
        <!-- </xsl:for-each> -->
        
      </body>
    </html>
  </xsl:template>

  <xsl:template match="title">
    <h2> <xsl:apply-templates /></h2>
  </xsl:template>

  <xsl:template match="italic">
    <i> <xsl:apply-templates /></i>
  </xsl:template>

  <xsl:template match="functions">
    <ul>
    <xsl:for-each select="symbol">
      <li class="functions">
        <xsl:apply-templates select="." />
      </li>
    </xsl:for-each>
    </ul>
  </xsl:template>

  <xsl:template match="detailedfunctions">
    <ul>
    <xsl:for-each select="symboldesc">
      <li><xsl:apply-templates select="symbol"/> - <xsl:value-of select="desc"/></li>
    </xsl:for-each>
    </ul>
  </xsl:template>


  <xsl:template match="description">
   <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="tutoriallink">
    <a><xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute><xsl:apply-templates/></a>
  </xsl:template>

  <xsl:template match="symbol">
    <a><xsl:attribute name="href"><xsl:value-of select="@link"/></xsl:attribute><xsl:apply-templates/></a>
  </xsl:template>


  <!-- <xsl:template match="keywords"> -->
    
  <!-- </xsl:template> -->

  <xsl:template match="packages">
    <xsl:apply-templates select="title|functions|detailedfunctions" />
  </xsl:template>
</xsl:stylesheet>


<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <title><xsl:value-of select="symbolref/symbolname"/></title>
        <link href="../../../../symbol.css" rel="stylesheet" type="text/css"/>
      </head>
      <body>
        <h1><xsl:value-of select="symbolref/symbolname"/></h1>
        <p class="usage"><xsl:apply-templates select="symbolref/usage"/></p>
        <xsl:apply-templates select="symbolref/moreinformation"/>
        <xsl:apply-templates select="symbolref/basicexamples"/>
        <xsl:apply-templates select="symbolref/seealso"/>
        <xsl:apply-templates select="symbolref/moreabout"/>
        
      </body>
    </html>
  </xsl:template>

  <xsl:template match="title">
    <h2> <xsl:apply-templates /></h2>
  </xsl:template>

  <xsl:template match="italic">
    <i> <xsl:apply-templates /></i>
  </xsl:template>

  <xsl:template match="functions">
    <ul>
    <xsl:for-each select="symbol">
      <li class="functions">
        <xsl:apply-templates select="." />
      </li>
    </xsl:for-each>
    </ul>
  </xsl:template>

  <xsl:template match="seealso">
    <h2>SEE ALSO</h2>
    <ul>
    <xsl:for-each select="symbol">
      <li class="functions">
        <xsl:apply-templates select="." />
      </li>
    </xsl:for-each>
    </ul>
  </xsl:template>

  <xsl:template match="basicexamples">
    <h2>EXAMPLES</h2>
    <h3>Basic Examples</h3>

    <table>
      <xsl:for-each select="example">
        <tr>
          <td class="label">In[1]:=</td> <td><xsl:apply-templates select="in" /></td>
        </tr>
        <tr>
          <td class="label">Out[1]:=</td><td><xsl:apply-templates select="out" /></td>
        </tr>
      </xsl:for-each>
    </table>

  </xsl:template>


  <xsl:template match="moreabout">
    <h2>MORE ABOUT</h2>
    <ul>
    <xsl:for-each select="guidelink">
      <li class="moreabout">
        <xsl:apply-templates select="." />
      </li>
    </xsl:for-each>
    </ul>
  </xsl:template>


  <xsl:template match="detailedfunctions">
    <ul>
    <xsl:for-each select="symboldesc">
      <li><xsl:apply-templates select="symbol"/> - <xsl:value-of select="desc"/></li>
    </xsl:for-each>
    </ul>
  </xsl:template>


  <xsl:template match="description">
   <xsl:apply-templates />
  </xsl:template>

  <!-- <xsl:template match="example"> -->
  <!--   <li><xsl:apply-templates /></li> -->
  <!-- </xsl:template> -->


  <xsl:template match="tutoriallink">
    <a><xsl:attribute name="href"><xsl:value-of select="@href"/></xsl:attribute><xsl:apply-templates/></a>
  </xsl:template>

  <xsl:template match="guidelink">
    <a class="guidelink"><xsl:attribute name="href">../../Guides/<xsl:value-of select="@href"/>.xml</xsl:attribute><xsl:apply-templates/></a>
  </xsl:template>

  <xsl:template match="symbol">
    <a><xsl:attribute name="href"><xsl:value-of select="@link"/></xsl:attribute><xsl:apply-templates/></a>
  </xsl:template>

  <xsl:template match="in">
    <p class="in"><xsl:apply-templates/></p>
  </xsl:template>

  <xsl:template match="out">
    <p>
      <img>
        <xsl:attribute name="src"><xsl:value-of select="@href"/>
        </xsl:attribute>
      </img>
    </p>
  </xsl:template>

  <xsl:template match="moreinformation">
    <h2 class="moreinfo">MORE INFORMATION</h2>
    <ul>
      <xsl:for-each select="info|options">
        <li class="moreinfo"><xsl:apply-templates/></li>
      </xsl:for-each>
    </ul>
  </xsl:template>

  <!-- <xsl:template match="keywords"> -->
    
  <!-- </xsl:template> -->

  <xsl:template match="packages">
    <xsl:apply-templates select="title|functions|detailedfunctions" />
  </xsl:template>

  <!-- TODO: This is supposed to cause a "More Information" heading to
       appear, but at the moment it doesn't.  I'm not sure of an easy
       way to do this. -->
  <xsl:template match="options">
    <p>The following options can be given:</p>
    <table class="options">
    <xsl:apply-templates/>
    </table>
  </xsl:template>

  <xsl:template match="option">
    <tr>
      <td class="option optionname"><xsl:value-of select="optionname"/></td>
      <td class="option optionvalue"><xsl:value-of select="optionvalue"/></td>
      <td class="option optiondescription"><xsl:value-of select="optiondescription"/></td>
    </tr>
  </xsl:template>

</xsl:stylesheet>


Paclet[
  Name -> "ApplicationTools",
  Version -> "0.1.0",
  MathematicaVersion -> "8+",
  Creator -> "Barry Wardell",
  Description -> "A set of tools for creating Mathematica applications.",
  Extensions -> {
    { "Kernel",
	  "Context" -> {"ApplicationTools`"}
	},

    {"Documentation",
     Language -> "English", 
     MainPage -> "Guides/DocumentationBuilder",
     Resources -> 
     	{"Guides/DocumentationBuilder"}
    }
  }
]

(* ::Package:: *)

BeginPackage["HTMLDoc`", {"DocumentationBuilder`"}];

GenerateHTMLSymbolReferencePage;
GenerateHTMLGuidePage;

Begin["`Private`"];

exportKey[key_String] :=
  StringReplace[ToLowerCase[key], " " -> ""];

export[key_String -> value_] :=
  {XMLElement[exportKey[key], {}, 
              export[value]]};

export[s_String] :=
  {s};

export[l_List] :=
  Flatten[Map[export, Riffle[l, " "]], 1];

export[other_] :=
  {XMLObject["CDATASection"][ToString[other]]};

export[key : "DetailedFunctions" -> table_List] :=
  XMLElement[exportKey[key], {},
             Map[XMLElement[
               "symboldesc", {}, {XMLElement[
                 "symbol", {"link" -> 
                            "/Documentation/English/ReferencePages/Symbols/" <> #[[1]] <>
                            ".xml"}, export[#[[1]]]], 
                                  XMLElement["desc", {}, 
                                             export[If[Length[#] === 2, 
                                                       #[[2]],
                                                       DocumentationBuilder`SymbolDescription[#[[1]]]]]]}] &, table]];

export[StyleBox[expr_, FontSlant -> "Italic"]] :=
  XMLElement["italic", {}, export[expr]];

export[TutorialLink[name_String, link_String]] :=
  XMLElement["tutoriallink", {"href" -> link}, export[name]];

exportSymbol[symbol_String] :=
  XMLElement[
    "symbol", {"link" -> FileNameJoin[{$symbolDocDir,symbol <> ".xml"}]}, export@symbol];

export["Functions" -> fns_List] :=
  XMLElement["functions", {}, exportSymbol /@ fns];

export["See Also" -> fns_List] :=
  XMLElement["seealso", {}, exportSymbol /@ fns];

export["More About" -> fns_List] :=
  XMLElement["moreabout", {}, exportGuideLink /@ fns];

exportGuideLink[s_] :=
  XMLElement["guidelink", {"href" -> s}, export[s]];

export["More Information" -> lines_List] :=
  XMLElement["moreinformation", {}, 
             XMLElement["info",{},export[#]] & /@ lines];


export["Basic Examples" -> examples_List] :=
  Module[
    {result},

    BeginPackage["Temp`", $ContextPath]; 

    result = XMLElement["basicexamples", {}, 
                        XMLElement["example", {}, exportExample@#] & /@ examples];
    EndPackage[];
    If[Names["Temp`*"] =!= {}, Print["Removing ", Names["Temp`*"]]; Remove["Temp`*"]];
    result];

getCounter[] := (counter++; counter);

exportExample[s_String] :=
  Module[
    {idx = ToString@getCounter[],
     filename},
    
    filename = FileNameJoin[{"images",$symbolName<>"_"<>idx<>".png"}];
    
    (* Export[FileNameJoin[{NotebookDirectory[], filename}], *)
    (*        , "PNG"]; *)

    Sow[filename -> ToExpression[s], PNGExpression];

    Flatten[
      {XMLElement["in", {}, export@s],
       XMLElement["out", {"href" -> filename}, {}]}]];

optDesc[opt_String] :=
  Module[
    {descs},
    If[!ListQ[DocumentationBuilder`OptionDescriptions[$symbolName]], Return[""]];
    descs = Cases[DocumentationBuilder`OptionDescriptions[$symbolName], (opt -> desc_)->desc];
    If[descs === {}, "", descs[[1]]]];

export["Options" -> options_List] :=
  If[Length[options] === 0, {}, {XMLElement[
    "options",{},
    Map[XMLElement["option",{},
                   {XMLElement["optionname",{},export[#[[1]]]],
                    XMLElement["optionvalue",{},{ToString[#[[2]]]}],
                    XMLElement["optiondescription", {}, export[optDesc[ToString@#[[1]]]]]}] &, options]]}];

GenerateHTMLSymbolReferencePage[symbolName_String, inputfile_String, docDir_String] :=
  Module[
    {outputfile, inputdata, usage, outputdata, symbolsDir, images, moreinfo},
    symbolsDir = FileNameJoin[{docDir,"English","ReferencePages","Symbols"}];
    outputfile = FileNameJoin[{symbolsDir, symbolName<>".xml"}];
    inputdata = If[FileType[inputfile] =!= None, 
                   Get[inputfile],
                   {}];

    usage = ToString[ToExpression[ToString[symbolName] <> "::usage"]];
    Quiet[CreateDirectory[symbolsDir, CreateIntermediateDirectories -> True],CreateDirectory::filex];
    Quiet[CreateDirectory[FileNameJoin[{symbolsDir,"images"}], CreateIntermediateDirectories -> True],CreateDirectory::filex];

    moreInfo = Flatten[Join[Cases[inputdata, ("More Information" -> x_) :> x],
                            If[Length[Options[Symbol[symbolName]]] > 0, 
                               {{"Options" -> Options[Symbol[symbolName]]}}, {}]],1];

    If[Length[moreInfo] > 0,
       inputdata = Append[DeleteCases[inputdata, "More Information" -> _], "More Information" -> moreInfo]];

    Block[
      {counter = 0, $symbolName = symbolName, $symbolDocDir = "."}, 
      {outputdata,images} = 
      Reap[
        XMLObject[
          "Document"][{XMLObject["ProcessingInstruction"]["xml-stylesheet", 
                                                          "href=\"../../../../symbol.xsl\" type=\"text/xsl\""]}, 
                      XMLElement["symbolref", {}, 
                                 export@Join[{"SymbolName" -> symbolName, 
                                              "Usage" -> usage}, 
                                             inputdata]], {}],
        PNGExpression]];

    If[Length[images] > 0,
       Scan[Export[FileNameJoin[{symbolsDir,#[[1]]}], #[[2]], "PNG"] &, images[[1]]]];
    Print["Writing ", outputfile];
    Export[outputfile, outputdata]];

GenerateHTMLGuidePage[guideName_String, inputfile_String, docDir_String] :=
  Module[
    {outputfile, inputdata, usage, outputdata, symbolsDir, images},
    guideDir = FileNameJoin[{docDir,"English","Guides"}];
    outputfile = FileNameJoin[{guideDir, guideName<>".xml"}];
    inputdata = If[FileType[inputfile] =!= None, 
                   Get[inputfile],
                   {}];

    Quiet[CreateDirectory[guideDir, CreateIntermediateDirectories -> True],CreateDirectory::filex];
    Quiet[CreateDirectory[FileNameJoin[{guideDir,"images"}], CreateIntermediateDirectories -> True],CreateDirectory::filex];
    Block[
      {counter = 0, $symbolName = symbolName,$symbolDocDir = "../ReferencePages/Symbols"}, 
      {outputdata,images} = 
      Reap[
        XMLObject[
          "Document"][{XMLObject["ProcessingInstruction"]["xml-stylesheet", 
                                                          "href=\"../../../guide.xsl\" type=\"text/xsl\""]}, 
                      XMLElement["guide", {}, 
                                 export[inputdata]], {}],
        PNGExpression]];

    Print["Temp names: ", Names["Temp`*"]];

    If[Length[images] > 0,
       Scan[Export[FileNameJoin[{guideDir,#[[1]]}], #[[2]], "PNG"] &, images[[1]]]];
    Print["Writing ", outputfile];
    Export[outputfile, outputdata]];



End[];
EndPackage[];

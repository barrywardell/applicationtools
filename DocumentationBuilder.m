(* ::Package:: *)

BeginPackage["DocumentationBuilder`",
  {"DocumentationSearch`"}];

BuildGuide::usage = "BuildGuide[source, target] builds the documentation notebook target from source.";
BuildTutorial::usage = "BuildTutorial[source, target] builds the documentation notebook target from source.";
BuildSymbolReference::usage = "BuildSymbolReference[app, symbol, source, target] builds a documentation page for symbol";
BuildIndex::usage = "BuildIndex[app] builds the documentation index for app.";

CreateDocumentationDocument::usage = "CreateDocumentationDocument[contents] creates a notebook document suitable for use in documentation creation.
CreateDocumentationDocument[] creates an empty notebook document suitable for use in documentation creation.";
MoreInformation::usage = "MoreInformation[symbol] is a list of documentation expressions for symbol.  They are each used in \"More Information\" section of symbol reference pages.";
OptionDescriptions::usage = "OptionDescriptions[symbol] is a list of descriptions of options for symbol.  They are each used in \"More Information\" section of symbol reference pages.";
TextComment::usage = "TextComment[string] specifies that string should be formatted as regular text in documentation.";
GuideTitle::usage = "GuideTitle[string] returns a cell for use as a title on a guide page.";

UndocumentedSymbols::usage = "UndocumentedSymbols[package] lists all public symbols in package which don't have usage messages defined.";
DocumentedSymbols::usage = "DocumentedSymbols[package] lists all public symbols in package which have usage messages defined.";

GuideSection::usage = "GuideSection[package, descriptions] generates a portion of a guide.md file for package giving detailed descriptions from descriptions which is a list of rules of the form symbolname -> description.";

TutorialLink::usage = "TutorialLink[app, tutorial] creates a link to the named tutorial in the given application.";

SymbolDescription;

Begin["`Private`"];

tutorialLinkRule = 
  TutorialLink[app_String, name_String] :>
  Cell[TextData[ButtonBox[name, BaseStyle->"Link", 
                          ButtonData->"paclet:"<>app<>"/tutorial/"<>name]]];

ifPreVer9[pre_, post_] := FEPrivate`If[
  FEPrivate`Or[FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"],
               FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"],
               FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
  pre, post];

appDir[package_] := Module[{dir},
  dir = FindFile[package];
  If[FileNameTake[dir, -2] == "Kernel/init.m",
    dir = FileNameDrop[dir, -2];
  ];
  If[StringMatchQ[FileNameTake[dir, -2], "Kernel/*.m"],
    dir = FileNameDrop[dir, -2];
  ];
  dir
];

getRule[rules_, rule_] := (rule /. rules) /. rule -> None;

sourceData[file_String] := Module[{rules},
  rules = Get[file] /.tutorialLinkRule;
  sourceData[rules]
];

sourceData[rules_List] := Module[{source},
  source = Unique["source"];
  source["Application"] = getRule[rules, "Application"];
  source["Package"]     = getRule[rules, "Package"];
  source["Title"]       = getRule[rules, "Title"];
  source["Summary"]     = getRule[rules, "Summary"];
  source["Description"] = getRule[rules, "Description"];
  source["Keywords"]    = getRule[rules, "Keywords"];
  source["Label"]       = getRule[rules, "Label"];
  source["Synonyms"]    = getRule[rules, "Synonyms"];
  source["URL"]         = getRule[rules, "URL"];
  source["Packages"]    = packageData/@getRule[rules, "Packages"];
  source["Tutorials"]   = getRule[rules, "Tutorials"];
  source[s_String]     := source[s] = getRule[rules, s];
  source
];

sourceData[None] := Module[{source},
  source = Unique["source"];
  source[s_String] = None;
  source
];

packageData[rules_] := Module[{package},
  package = Unique["package"];
  package["Title"]             = getRule[rules, "Title"];
  package["Link"]              = getRule[rules, "Link"];
  package["DetailedFunctions"] = getRule[rules, "DetailedFunctions"];
  package["Functions"]         = getRule[rules, "Functions"];
  package["MoreFunctionsLink"] = getRule[rules, "MoreFunctionsLink"];
  package
]

symbolLink[app_String, name_String] :=
  ButtonBox[name, BaseStyle -> "Link", ButtonData -> "paclet:"<>app<>"/ref/"<>name];

tutorialLink[app_String, name_String] :=
  ButtonBox[name, BaseStyle -> "Link", ButtonData -> "paclet:"<>app<>"/tutorial/"<>name];

guideLink[app_String, name_String] :=
  ButtonBox[name, BaseStyle -> "Link", ButtonData -> "paclet:"<>app<>"/guide/"<>name];

detailedFunctionDescription[{name_, description_:Automatic}, app_] :=
  Cell[TextData[{Cell[TextData[symbolLink[app, name]], "InlineFunctionSans"],
   " ",
   StyleBox["\[LongDash]", "GuideEmDash"],
   " ",
   If[description === Automatic, SymbolDescription[name], description]
  }], "GuideText"];

GuideTitle[string_] := GuideTitle[string, None]

GuideTitle[string_, link_] :=
  If[SameQ[link, None],
    Cell[TextData[string], "GuideFunctionsSubsection"]
  ,
    Cell[TextData[ButtonBox[string<>" \[RightGuillemet]",
	  BaseStyle -> {"Link", "GuideFunctionsSubsection"},
	  ButtonData -> link]
	  ], "GuideFunctionsSubsection"]
  ];

buildPackageDescription[app_, package_] := Module[{title, detailed, short, functionsList},
  title = GuideTitle[package["Title"], package["Link"]];

  (* Functions including a descriptive text *)
  If[!SameQ[package["DetailedFunctions"], None],
    Module[{fns = package["DetailedFunctions"]},
    Scan[If[!SymbolDocumentedQ[#],
            Print[Style["WARNING (1): Skipped reference in guide \""<>package["Title"]<>"\" to undocumented symbol "<>#, Red]]] &,
         First/@fns];

    fns = Select[fns, SymbolDocumentedQ[#[[1]]] &];

    detailed = Map[detailedFunctionDescription[#, app["Package"]]&, fns]]
  ,
    detailed = Sequence[];
  ];

  (* Functions without a description *)
  If[!SameQ[package["Functions"], None] && !SameQ[package["Functions"], {}],
    Module[{fns = package["Functions"]},

    Scan[If[!SymbolDocumentedQ[#],
            Print[Style["WARNING (2): Skipped reference in guide \""<>package["Title"]<>"\" to undocumented symbol "<>#, Red]]] &,
         fns];

    fns = Select[fns, SymbolDocumentedQ];
    functionsList = Map[Cell[TextData[symbolLink[app["Application"], #]], "InlineFunctionSans"]&, fns];

    If[!SameQ[package["MoreFunctionsLink"], None],
      AppendTo[functionsList, Cell[TextData[ButtonBox["...", BaseStyle -> "Link",
        ButtonData -> package["MoreFunctionsLink"]]], "InlineFunctionSans"]];
    ];

    short = Cell[TextData[Flatten[Riffle[
      functionsList,
      {{"\[NonBreakingSpace]", StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", "InlineSeparator"], " "}}
      ]]], "InlineGuideFunctionListing"]];
  ,
    short = Sequence[];
  ];

  Cell[CellGroupData[Flatten[{title, detailed, short}], Open]]
];

BuildGuide[sourceRules_, target_] :=
 Module[
  {source, nb, tutorialLinks},

  source = sourceData[sourceRules];

  If[FileExistsQ[target], DeleteFile[target]];

  nb = CreateDocument[Null, Visible -> False];

  SetOptions[nb,
    Saveable -> False,
    ScreenStyleEnvironment -> "Working",
    WindowSize -> {725, 750},
    WindowMargins -> {{0, Automatic}, {Automatic, 0}},
    WindowTitle -> source["Title"],
    TaggingRules -> {
      "ModificationHighlight"->False,
      "Metadata"-> {
        "context"     -> source["Application"]<>"`",
        "keywords"    -> source["Keywords"],
        "index"       -> True,
        "label"       -> source["Label"],
        "language"    -> "en",
        "paclet"      -> source["Package"],
        "status"      -> "None",
        "summary"     -> source["Summary"],
        "synonyms"    -> source["Synonyms"],
        "title"       -> source["Title"],
        "windowTitle" -> source["Title"],
        "type"        -> "Guide",
        "uri"         -> source["Application"] <> "/guide/" <> source["Package"]
      },
      "SearchTextTranslated"->""},
    FrontEndVersion -> $Version,
    PrivateNotebookOptions -> {"FileOutlineCache" -> False},
    "TrackCellChangeTimes" -> False
  ];

  (* Add header *)
  NotebookWrite[nb, Cell[" ", "GuideColorBar", CellMargins->{{Inherited, Inherited}, {-5, 0}}]];
  NotebookWrite[nb, Cell[TextData[{
    ButtonBox[source["Application"], BaseStyle->{"Link", "LinkTrail"}, ButtonData->"paclet:"<>source["Application"]<>"/guide/"<>source["Application"]],
    StyleBox[" > ", "LinkTrailSeparator"],
    ButtonBox[source["Title"], BaseStyle->{"Link", "LinkTrail"}, ButtonData->"paclet:"<>source["Application"]<>"/guide/"<>source["Package"]]
    }], "LinkTrail"]];

  (* Add top bar including links *)
  If[!SameQ[source["Tutorials"], None],
    tutorialLinks = Map["paclet:"<>source["Application"]<>"/tutorial/"<>#&, source["Tutorials"]];
    tutorialLinkCells = Sequence[Cell[BoxData[ActionMenuBox[FrameBox["\<\"Tutorials \[RightGuillemet]\"\>", StripOnInput->False],
        MapThread[#1 :> Documentation`HelpLookup[#2]&, {source["Tutorials"], tutorialLinks}],
        Appearance -> None, MenuAppearance -> Automatic]], LineSpacing->{1.4,0}],
        "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]"];
  ,
    tutorialLinkCells = Sequence[];
  ];

  NotebookWrite[nb,
    Cell[BoxData[GridBox[
      {{Cell[ToUpperCase[source["Package"]]<>" GUIDE", "PacletNameCell"],
        Cell[TextData[{
          tutorialLinkCells,
          Cell[BoxData[ActionMenuBox[FrameBox["\<\"URL \[RightGuillemet]\"\>", StripOnInput->False],
            {(#1 :> FrontEndExecute[{NotebookLocate[{URL[#2], None}]}])&
              ["\<\"Go to "<>source["Application"]<>" website\"\>", source["URL"]],
             Delimiter,
             "\<\"Go to wolfram Mathematica website\"\>" :>
               FrontEndExecute[{NotebookLocate[{
                 URL["http://reference.wolfram.com/mathematica/guide/Mathematica.html"], None}]}]
            },
            Appearance->None,
            MenuAppearance->Automatic
          ]], LineSpacing->{1.4,0}]
        }], "AnchorBar"]
       }}
    ]], "AnchorBarGrid"]
  ];

  (* Title and description *)
  NotebookWrite[nb, Cell[TextData[source["Title"]], "GuideTitle"]];
  NotebookWrite[nb, Cell[TextData[source["Description"]], "GuideAbstract"]];

  (* Packages *)
  If[!SameQ[source["Packages"], None],
    NotebookWrite[nb,
      Cell[CellGroupData[Map[buildPackageDescription[source, #]&, source["Packages"]], Open]]
    ];
  ];

  (* Tutorials section *)
  If[!SameQ[source["Tutorials"], None],
    NotebookWrite[nb, Cell[CellGroupData[Flatten[
      {Cell["", "GuideTutorialsSection",
         CellFrameLabelMargins -> 0,
         CellFrameLabels -> {{ifPreVer9[
           Cell["TUTORIALS", "GuideTutorialsSection"],
           Cell["Tutorials", "GuideTutorialsSection"]],
           None}, {None, None}}],
       Map[Cell[TextData[StyleBox[tutorialLink[source["Application"], #]]], "GuideTutorial"]&, source["Tutorials"]]
      }], Open]]
    ];
  ];

  NotebookWrite[nb, Cell[" ","FooterCell"]];

  SetOptions[nb,
    Visible -> True,
    StyleDefinitions -> FrontEnd`FileName[{"Wolfram"}, "Reference.nb", CharacterEncoding -> "UTF-8"]
  ];

   dir=FileNameDrop[target, -1];
   Print["Creating ", dir];
   If[FileType[dir] =!= Directory,
      CreateDirectory[dir, CreateIntermediateDirectories->True]];


  (* Save notebook *)
  NotebookSave[nb, target];
  NotebookClose[nb];
];

BuildTutorial[sourceRules_, target_:Automatic] :=
 Module[
  {contents = {}, contentsFile, tutorialLinkCells, sourcenb, targetnb, targetFile, source},

  source = sourceData[sourceRules];

  (* Read the contents from a nb file, if specified *)
  If[!SameQ[source["ContentsFile"], None],
    contentsFile = FileNameJoin[{DirectoryName[sourceRules], source["ContentsFile"]}];
  ,
    contentsFile = FileNameJoin[{DirectoryName[sourceRules], FileBaseName[sourceRules]<>".nb"}];
  ];

  If[FileExistsQ[contentsFile],
    sourcenb = NotebookOpen[contentsFile, Visible -> False];
    source["Contents"] = First[NotebookGet[sourcenb]];
    NotebookClose[sourcenb];
  ];

  (* Header *)
  AppendTo[contents,
    Cell[" ", "TutorialColorBar", CellMargins->{{Inherited, Inherited}, {-5, 0}}]];

  AppendTo[contents, Cell[TextData[{
    ButtonBox[source["Application"], BaseStyle->{"Link", "LinkTrail"}, ButtonData->"paclet:"<>source["Application"]<>"/guide/"<>source["Application"]],
    StyleBox[" > ", "LinkTrailSeparator"],
    ButtonBox[source["Title"], BaseStyle->{"Link", "LinkTrail"}, ButtonData->"paclet:"<>source["Application"]<>"/tutorial/"<>source["Title"]]
    }], "LinkTrail"]];

  (* Top bar including links *)
  If[!SameQ[source["Related Tutorials"], None],
    tutorialLinks = Map["paclet:"<>source["Application"]<>"/tutorial/"<>#&, source["Related Tutorials"]];
    tutorialLinkCells = Sequence[Cell[BoxData[ActionMenuBox[FrameBox["\<\"Related Tutorials \[RightGuillemet]\"\>", StripOnInput->False],
        MapThread[#1 :> Documentation`HelpLookup[#2]&, {source["Related Tutorials"], tutorialLinks}],
        Appearance -> None, MenuAppearance -> Automatic]], LineSpacing->{1.4,0}],
        "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]"];
  ,
    tutorialLinkCells = Sequence[];
  ];

  AppendTo[contents,
    Cell[BoxData[GridBox[
      {{Cell[ToUpperCase[source["Package"]]<>" TUTORIAL", "PacletNameCell"],
        Cell[TextData[{
          tutorialLinkCells,
          Cell[BoxData[ActionMenuBox[FrameBox["\<\"URL \[RightGuillemet]\"\>", StripOnInput->False],
            {(#1 :> FrontEndExecute[{NotebookLocate[{URL[#2], None}]}])&
              ["\<\"Go to "<>source["Application"]<>" website\"\>", source["URL"]],
             Delimiter,
             "\<\"Go to wolfram Mathematica website\"\>" :>
               FrontEndExecute[{NotebookLocate[{
                 URL["http://reference.wolfram.com/mathematica/guide/Mathematica.html"], None}]}]
            },
            Appearance->None,
            MenuAppearance->Automatic
          ]], LineSpacing->{1.4,0}]
        }], "AnchorBar"]
       }}
    ]], "AnchorBarGrid"]
  ];

  (* Main body of tutorial *)
  AppendTo[contents,
    Sequence@@source["Contents"]];

  (* Tutorials section *)
  If[!SameQ[source["Related Tutorials"], None],
    AppendTo[contents, Cell[CellGroupData[Flatten[
      {Cell["", "RelatedTutorialsSection", System`WholeCellGroupOpener->True,
        CellFrameLabelMargins -> 0,
        CellFrameLabels -> {{ifPreVer9[
          Cell[TextData[ButtonBox[
                  "RELATED TUTORIALS",
                  BaseStyle -> None,
                  Appearance -> {Automatic, None},
                  Evaluator->None,
                  Method->"Preemptive",
                  ButtonFunction :>
                    (FrontEndExecute[{FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], All, ButtonCell], FrontEndToken["OpenCloseGroup"], FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], After, CellContents]}]&)
                  ]], "RelatedTutorialsSection"]
          ,
          TextData[ButtonBox[
                  Cell[TextData[{Cell[BoxData[ToBoxes[Spacer[24]]]], "Related Tutorials"}], "RelatedTutorialsSection"],
                  BaseStyle -> None,
                  Appearance -> {Automatic, None},
                  Evaluator->None,
                  Method->"Preemptive",
                  ButtonFunction :>
                    (FrontEndExecute[{FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], All, ButtonCell], FrontEndToken["OpenCloseGroup"], FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], After, CellContents]}]&)
                  ]]],
          None}, {None, None}}],

       Map[Cell[TextData[StyleBox[tutorialLink[source["Application"], #]]], "RelatedTutorials"]&, source["Related Tutorials"]]
      }], Open]]
    ];
  ];

  AppendTo[contents,  Cell[" ","FooterCell"]];

  If[SameQ[target, Automatic],
    targetFile = FileNameJoin[{appDir[source["Application"]<>"`"], "Documentation", "English", "Tutorials", FileBaseName[sourceRules]<>".nb"}];,
    targetFile = target;
  ];

  If[FileExistsQ[targetFile], DeleteFile[targetFile]];

  targetnb = CreateDocument[contents, Visible -> False];

  SetOptions[targetnb,
    Saveable -> False,
    ScreenStyleEnvironment -> "Working",
    WindowSize -> {725, 750},
    WindowMargins -> {{0, Automatic}, {Automatic, 0}},
    WindowTitle -> source["Title"],
    TaggingRules -> {
      "ModificationHighlight"->False,
      "Metadata"-> {
        "context"     -> source["Application"]<>"`",
        "keywords"    -> source["Keywords"],
        "index"       -> True,
        "label"       -> source["Label"],
        "language"    -> "en",
        "paclet"      -> source["Package"],
        "status"      -> "None",
        "summary"     -> source["Summary"],
        "synonyms"    -> source["Synonyms"],
        "title"       -> source["Title"],
        "windowTitle" -> source["Title"],
        "type"        -> "Tutorial",
        "uri"         -> source["Application"] <> "/tutorial/" <> source["Title"]
      },
      "SearchTextTranslated"->""},
    FrontEndVersion -> $Version,
    PrivateNotebookOptions -> {"FileOutlineCache" -> False},
    "TrackCellChangeTimes" -> False
  ];

  (* Save notebook *)
  SetOptions[targetnb, Visible -> True];

  BeginPackage["Temp`", $ContextPath]; 
  NotebookEvaluate[targetnb, InsertResults -> True];
  EndPackage[];
  If[Names["Temp`*"] =!= {}, Print["Removing ", Names["Temp`*"]]; ClearAll["Temp`*"]];

  SetOptions[targetnb,
    StyleDefinitions -> FrontEnd`FileName[{"Wolfram"}, "Reference.nb", CharacterEncoding -> "UTF-8"]];
  NotebookSave[targetnb, targetFile];
  NotebookClose[targetnb];
];

BuildSymbolReference[app_, symbol_String, sourceRules_:None, target_:Automatic] :=
 Module[
  {source, sourceFile, nb, nbFile, tutorialLinks, seeAlsoLinks, moreAboutLinks,
   tutorialLinkCells, seeAlsoLinkCells, moreAboutLinkCells, urlLinkCells, linkCells,
   usageMessages, moreInfoCells, symbolOptions, basicExamples, ignoredOptions,
   documentedOptions, optionsTable, optionsCell},

  Which[
    sourceRules === None,
      source = sourceData[None];,
    ListQ[sourceRules],
      source = sourceData[sourceRules];,
    FileExistsQ[sourceRules] && FileType[sourceRules] === File,
      source = sourceData[sourceRules];,
    FileExistsQ[sourceFile=FileNameJoin[{sourceRules, symbol<>".md"}]],
      source = sourceData[sourceFile];,
    FileExistsQ[sourceFile=FileNameJoin[{sourceRules, "Documentation", "English", "ReferencePages", "Symbols", symbol<>".md"}]],
      source = sourceData[sourceFile];,
    True,
      source = sourceData[None];
  ];

  If[SameQ[source["Application"], None],
    source["Application"] = app;
  ];

  If[SameQ[source["Package"], None],
    source["Package"] = ToString[StringDrop[Context[symbol], -1]];
  ];

  If[SameQ[source["Symbol"], None],
    source["Symbol"] = symbol;
  ];

  If[SameQ[source["Usage"], None],
    source["Usage"] = ToExpression[source["Symbol"]<>"::usage"];
  ];

  If[SameQ[source["Keywords"], None],
    source["Keywords"] = {source["Symbol"], ToUpperCase[source["Symbol"]], ToLowerCase[source["Symbol"]]};
  ];

  If[SameQ[source["Synonyms"], None],
    source["Synonyms"] = {source["Symbol"], ToUpperCase[source["Symbol"]], ToLowerCase[source["Symbol"]]};
  ];

  If[SameQ[source["Label"], None],
    source["Label"] = source["Application"]<>"/"<>source["Package"]<>" Symbol";
  ];

  If[SameQ[source["Summary"], None],
    source["Summary"] = source["Usage"];
  ];

  If[SameQ[source["Option Descriptions"], None] && ValueQ[OptionDescriptions[symbol]],
    source["Option Descriptions"] = OptionDescriptions[symbol];
  ];

  If[SameQ[source["More Information"], None] && ValueQ[MoreInformation[symbol]],
    source["More Information"] = MoreInformation[symbol];
  ];

  If[SameQ[target, Automatic],
    nbFile = FileNameJoin[{appDir[app<>"`"], "Documentation", "English", "ReferencePages", "Symbols", source["Symbol"]<>".nb"}];,
    nbFile = target;
  ];

  If[FileExistsQ[nbFile], DeleteFile[nbFile]];

  nb = CreateDocument[Null, Visible -> False];

  SetOptions[nb,
    Saveable -> False,
    ScreenStyleEnvironment -> "Working",
    WindowSize -> {725, 750},
    WindowMargins -> {{0, Automatic}, {Automatic, 0}},
    WindowTitle -> source["Symbol"],
    TaggingRules -> {
      "ModificationHighlight"->False,
      "Metadata"-> {
        "context"     -> app<>"`",
        "keywords"    -> source["Keywords"],
        "index"       -> True,
        "label"       -> source["Label"],
        "language"    -> "en",
        "paclet"      -> source["Package"],
        "status"      -> "None",
        "summary"     -> source["Summary"],
        "synonyms"    -> source["Synonyms"],
        "title"       -> source["Symbol"],
        "windowTitle" -> source["Symbol"],
        "type"        -> "Symbol",
        "uri"         -> source["Application"] <> "/ref/" <> source["Symbol"]
      },
      "SearchTextTranslated"->""},
    FrontEndVersion -> $Version,
    PrivateNotebookOptions -> {"FileOutlineCache" -> False},
    "TrackCellChangeTimes" -> False
  ];

  (* Add header *)
  NotebookWrite[nb, Cell[" ", "SymbolColorBar", CellMargins->{{Inherited, Inherited}, {-5, 0}}]];
  NotebookWrite[nb, Cell[TextData[{
    ButtonBox[source["Application"], BaseStyle->{"Link", "LinkTrail"}, ButtonData->"paclet:"<>source["Application"]<>"/guide/"<>source["Application"]],
    StyleBox[" > ", "LinkTrailSeparator"],
    ButtonBox[source["Package"], BaseStyle->{"Link", "LinkTrail"}, ButtonData->"paclet:"<>source["Application"]<>"/guide/"<>source["Package"]],
    StyleBox[" > ", "LinkTrailSeparator"],
    ButtonBox[source["Symbol"], BaseStyle->{"Link", "LinkTrail"}, ButtonData->"paclet:"<>source["Application"]<>"/ref/"<>source["Symbol"]]
    }], "LinkTrail"]];

  (* Add top bar including links, see also, etc. *)
  If[!SameQ[source["Tutorials"], None],
    tutorialLinks = Map["paclet:"<>source["Application"]<>"/tutorial/"<>#&, source["Tutorials"]];
    tutorialLinkCells = Sequence[Cell[BoxData[ActionMenuBox[FrameBox["\<\"Tutorials \[RightGuillemet]\"\>", StripOnInput->False],
        MapThread[#1 :> Documentation`HelpLookup[#2]&, {source["Tutorials"], tutorialLinks}],
        Appearance -> None, MenuAppearance -> Automatic]], LineSpacing->{1.4,0}],
        "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]"];
  ,
    tutorialLinkCells = Sequence[];
  ];

  If[!SameQ[source["See Also"], None],
    seeAlsoLinks = Map["paclet:"<>source["Application"]<>"/ref/"<>#&, source["See Also"]];
    seeAlsoLinkCells = Sequence[Cell[BoxData[ActionMenuBox[FrameBox["\<\"See Also \[RightGuillemet]\"\>", StripOnInput->False],
        MapThread[#1 :> Documentation`HelpLookup[#2]&, {source["See Also"], seeAlsoLinks}],
        Appearance -> None, MenuAppearance -> Automatic]], LineSpacing->{1.4,0}],
        "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]"];
  ,
    seeAlsoLinkCells = Sequence[];
  ];

  If[!SameQ[source["More About"], None],
    moreAboutLinks = Map["paclet:"<>source["Application"]<>"/guide/"<>#&, source["More About"]];
    moreAboutLinkCells = Sequence[Cell[BoxData[ActionMenuBox[FrameBox["\<\"More About \[RightGuillemet]\"\>", StripOnInput->False],
        MapThread[#1 :> Documentation`HelpLookup[#2]&, {source["More About"], moreAboutLinks}],
        Appearance -> None, MenuAppearance -> Automatic]], LineSpacing->{1.4,0}],
        "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]"];
  ,
    moreAboutLinkCells = Sequence[];
  ];

  If[!SameQ[source["URL"], None],
    urlLinkCells =
      Cell[BoxData[ActionMenuBox[FrameBox["\<\"URL \[RightGuillemet]\"\>", StripOnInput->False],
        {(#1 :> FrontEndExecute[{NotebookLocate[{URL[#2], None}]}])&
          ["\<\"Go to "<>source["Application"]<>" website\"\>", source["URL"]],
         Delimiter,
         "\<\"Go to wolfram Mathematica website\"\>" :>
           FrontEndExecute[{NotebookLocate[{
             URL["http://reference.wolfram.com/mathematica/guide/Mathematica.html"], None}]}]
        },
        Appearance->None,
        MenuAppearance->Automatic
      ]], LineSpacing->{1.4,0}];
  ,
    urlLinkCells = Sequence[];
  ];

  linkCells = {tutorialLinkCells, seeAlsoLinkCells, moreAboutLinkCells, urlLinkCells};

  If[linkCells != {},
    NotebookWrite[nb,
      Cell[BoxData[GridBox[
        {{Cell[ToUpperCase[source["Package"]]<>" PACLET SYMBOL", "PacletNameCell"], Cell[TextData[linkCells], "AnchorBar"]}}
      ]], "AnchorBarGrid"]
    ];
  ];

  (* Title *)
  NotebookWrite[nb, Cell[source["Symbol"], "ObjectName"]];

  (* Usage message *)
  usageMessages = StringSplit[source["Usage"], {"\n"}];
  usageMessages =
    StringCases[
      usageMessages,
      {source["Symbol"] <> "[" ~~ x : Except["]"] ... ~~ "] " ~~ y___ :>
        {"",
         Cell[TextData[{Cell[BoxData[RowBox[{symbolLink[source["Application"], source["Symbol"]], "[", StyleBox[x, "TI"], "]"}]], "InlineFormula"], "\[LineSeparator]" <> y}]]
        },
       source["Symbol"] ~~ y___ :>
        {"",
         Cell[TextData[{Cell[TextData[symbolLink[source["Application"], source["Symbol"]]], "InlineFormula"], "\[LineSeparator]" <> y}]]
        },
       x___ :> {"", Cell[TextData[{x}]]}
      },
      1
    ][[All, 1]];

  NotebookWrite[nb,
    Cell[BoxData[GridBox[usageMessages]], "Usage"]
  ];

  (* Options *)
  If[source["Inherited Options"] != None,
    ignoredOptions = Flatten[Options /@ source["Inherited Options"]][[All, 1]];
  ,
    ignoredOptions = {}
  ];

  symbolOptions = Complement[Options[Symbol[symbol]][[All,1]], ignoredOptions];
  documentedOptions = source["Option Descriptions"] /. None -> {};

  If[!SameQ[ToString/@Union@symbolOptions, ToString/@Union[documentedOptions[[All,1]]]],
    Print[Style["Documented options "<>ToString[documentedOptions[[All,1]]]<>" do not match actual options "<>ToString[symbolOptions]<>" for "<>source["Symbol"], Red]];
  ];

  If[!SameQ[symbolOptions, {}],
    optionsTable = Map[{Cell["      ", "TableRowIcon"],
      ToString[#], ToString[# /. Options[Symbol[symbol]]],
        Cell[ToString[# /. documentedOptions /. #->""], "TableText"]}&, symbolOptions];
    optionsCell = {Cell["The following options can be given: ", "Notes"],
      Cell[BoxData[GridBox[optionsTable,
        GridBoxAlignment->{"Columns" -> {Left, Left, {Left}},
        "ColumnsIndexed" -> {},
        "Rows" -> {{Baseline}},
        "RowsIndexed" -> {}}]],
      "3ColumnTableMod",
      GridBoxOptions->{
        GridBoxBackground->{
          "Columns" -> {{None}},
          "ColumnsIndexed" -> {},
          "Rows" -> Join[ConstantArray[None, Length[symbolOptions]-1], {{None}}],
          "RowsIndexed" -> {}
        },
        GridBoxDividers->{
          "Rows" -> {ConstantArray[True, Length[symbolOptions]+1]}
        }
      }
      ]};
  ,
    optionsCell = {};
  ];

  (* More info *)
  If[!SameQ[source["More Information"], None] || optionsCell != {},
    moreInfoCells = Sequence@@(Cell[#, "Notes"]& /@ (source["More Information"]/. None -> {}));
    NotebookWrite[nb,
      Cell[CellGroupData[{
        Cell["", "NotesSection", CellGroupingRules -> {"SectionGrouping", 50}, "WholeCelGroupOpener" -> True, 
         CellFrameLabelMargins -> 0,
         CellFrameLabels -> {{ifPreVer9[
           Cell[
             TextData[Cell[BoxData[ButtonBox[
               FrameBox[StyleBox[RowBox[{"MORE", " ", "INFORMATION"}], "NotesFrameText"], StripOnInput -> False],
               Appearance -> {Automatic, None},
               BaseStyle -> None,
               ButtonFunction :>
                 (FrontEndExecute[{FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], All, ButtonCell],
                   FrontEndToken["OpenCloseGroup"],
                   FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], After, CellContents]}]& ),
               Evaluator -> None,
               Method -> "Preemptive"]]]]
             , "NotesSection"],
           TextData[ButtonBox[
               Cell[TextData[{Cell[BoxData[ToBoxes[Spacer[24]]]], "Details and Options"}], "NotesSection"],
               Appearance -> {Automatic, None},
               BaseStyle -> None,
               ButtonFunction :>
                 (FrontEndExecute[{FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], All, ButtonCell],
                  FrontEndToken["OpenCloseGroup"],
                  FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], After, CellContents]}]& ),
               Evaluator -> None,
               Method -> "Preemptive"]]
            ], None}, {None, None}}],
        moreInfoCells,
        Sequence@@optionsCell
      }, Open]]
    ];
  ];

  BeginPackage["Temp`", $ContextPath]; 

  (* Examples *)
  If[!SameQ[source["Basic Examples"], None],
    basicExamples = Sequence@@((# /. {TextComment[ex_] :> Cell[TextData[{ex}], "ExampleText"], ex_ :> Cell[CellGroupData[{Cell[ex,"Input", CellLabel -> "In[1]:="], Cell[BoxData[ToBoxes[ToExpression[ex]]], "Output", CellLabel -> "Out[1]:= "]}, Open]]}) & /@ source["Basic Examples"]);
    NotebookWrite[nb,
      Cell[CellGroupData[{
        Cell["", "PrimaryExamplesSection", CellTags->"PrimaryExamplesSection",
          CellFrameLabelMargins -> 0,
          CellFrameLabels -> {{ifPreVer9[
            Cell[TextData[ButtonBox["EXAMPLES", BaseStyle -> None, Appearance -> {Automatic, None},
              Evaluator -> None, Method -> "Preemptive",
              ButtonFunction :>
                (FrontEndExecute[{FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], All, ButtonCell], FrontEndToken["OpenCloseGroup"], FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], After, CellContents]}]&)
            ]], "PrimaryExamplesSection", CellTags->"PrimaryExamplesSection"],
            TextData[ButtonBox[Cell[TextData[{Cell[BoxData[ToBoxes[Spacer[24]]]], "Examples"}], "PrimaryExamplesSection", CellTags->"PrimaryExamplesSection"], BaseStyle -> None, Appearance -> {Automatic, None},
              Evaluator -> None, Method -> "Preemptive",
              ButtonFunction :>
                (FrontEndExecute[{FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], All, ButtonCell], FrontEndToken["OpenCloseGroup"], FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], After, CellContents]}]&)
            ]]],
            None}, {None, None}}],

        Cell[CellGroupData[{
          Cell[TextData[{"Basic Examples", "\[NonBreakingSpace]\[NonBreakingSpace]",
            Cell["("<>ToString[Length[source["Basic Examples"]]]<>")", "ExampleCount"]}], "ExampleSection"],
          basicExamples
        }, Open  ]]
      }, Open  ]]
    ];
  ];

  EndPackage[];
  If[Names["Temp`*"] =!= {}, Print["Removing ", Names["Temp`*"]]; ClearAll["Temp`*"]];

  (* See Also *)
  If[!SameQ[source["See Also"], None],
    seeAlso = Sequence@@Flatten[Riffle[
      Cell[BoxData[StyleBox[symbolLink[source["Application"], #], FontFamily->"Verdana"]], "InlineFormula"]& /@ source["See Also"],
      {{ "\[NonBreakingSpace]", StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", "InlineSeparator"], " "}}]];
    NotebookWrite[nb,
      Cell[CellGroupData[{
        Cell["", "SeeAlsoSection",
          CellFrameLabelMargins -> 0,
          CellFrameLabels -> {{ifPreVer9[
          Cell[TextData[ButtonBox[
            "SEE ALSO",
            BaseStyle -> None,
            Appearance -> {Automatic, None},
            Evaluator->None,
            Method->"Preemptive",
            ButtonFunction :>
              (FrontEndExecute[{FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], All, ButtonCell], FrontEndToken["OpenCloseGroup"], FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], After, CellContents]}]&)
          ]], "SeeAlsoSection"],
          TextData[ButtonBox[
            Cell[TextData[{Cell[BoxData[ToBoxes[Spacer[24]]]], "See Also"}], "SeeAlsoSection"],
            BaseStyle -> None,
            Appearance -> {Automatic, None},
            Evaluator->None,
            Method->"Preemptive",
            ButtonFunction :>
              (FrontEndExecute[{FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], All, ButtonCell], FrontEndToken["OpenCloseGroup"], FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], After, CellContents]}]&)
          ]]],
        None}, {None, None}}],

        Cell[TextData[seeAlso], "SeeAlso"]
      }, Open]]
    ];
  ];

  (* Tutorials section *)
  If[!SameQ[source["Tutorials"], None],
    NotebookWrite[nb, Cell[CellGroupData[Flatten[
      {Cell["", "TutorialsSection", System`WholeCellGroupOpener->True,
        CellFrameLabelMargins -> 0,
        CellFrameLabels -> {{ifPreVer9[
        Cell[TextData[ButtonBox[
          "TUTORIALS",
          BaseStyle -> None,
          Appearance -> {Automatic, None},
          Evaluator->None,
          Method->"Preemptive",
          ButtonFunction :>
            (FrontEndExecute[{FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], All, ButtonCell], FrontEndToken["OpenCloseGroup"], FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], After, CellContents]}]&)
          ]], "TutorialsSection"],
        TextData[ButtonBox[
          Cell[TextData[{Cell[BoxData[ToBoxes[Spacer[24]]]], "Tutorials"}], "TutorialsSection"],
          BaseStyle -> None,
          Appearance -> {Automatic, None},
          Evaluator->None,
          Method->"Preemptive",
          ButtonFunction :>
            (FrontEndExecute[{FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], All, ButtonCell], FrontEndToken["OpenCloseGroup"], FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], After, CellContents]}]&)
          ]]],
        None}, {None, None}}],

       Map[Cell[TextData[StyleBox[tutorialLink[source["Application"], #]]], "Tutorials"]&, source["Tutorials"]]
      }], Open]]
    ];
  ];

  (* More About *)
  If[!SameQ[source["More About"], None],
    moreAbout = Sequence@@(Cell[TextData[guideLink[source["Application"], #]], "MoreAbout"] & /@ source["More About"]);
    NotebookWrite[nb,
      Cell[CellGroupData[{
        Cell["", "MoreAboutSection",
          CellFrameLabelMargins -> 0,
          CellFrameLabels -> {{ifPreVer9[
          Cell[TextData[ButtonBox[
            "MORE ABOUT",
            BaseStyle->None,
            Appearance->{Automatic, None},
            Evaluator->None,
            Method->"Preemptive",
            ButtonFunction :>
              (FrontEndExecute[{FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], All, ButtonCell], FrontEndToken["OpenCloseGroup"], FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], After, CellContents]}]&)
          ]], "MoreAboutSection"],
          TextData[ButtonBox[
            Cell[TextData[{Cell[BoxData[ToBoxes[Spacer[24]]]], "More About"}], "MoreAboutSection"],
            BaseStyle->None,
            Appearance->{Automatic, None},
            Evaluator->None,
            Method->"Preemptive",
            ButtonFunction :>
              (FrontEndExecute[{FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], All, ButtonCell], FrontEndToken["OpenCloseGroup"], FrontEnd`SelectionMove[FrontEnd`SelectedNotebook[], After, CellContents]}]&)
          ]]],
        None}, {None, None}}],

        moreAbout
      }, Open]]
    ];
  ];

  NotebookWrite[nb, Cell[" ","FooterCell"]];

  SetOptions[nb,
    Visible -> True,
    StyleDefinitions -> FrontEnd`FileName[{"Wolfram"}, "Reference.nb", CharacterEncoding -> "UTF-8"]
  ];

  (* Save notebook *)
  dir=FileNameDrop[nbFile, -1];
  If[FileType[dir] =!= Directory,
     CreateDirectory[dir, CreateIntermediateDirectories->True]];
  NotebookSave[nb, nbFile];
  NotebookClose[nb];
];

CreateDocumentationDocument[contents_:Null] :=
 Module[{nb},
  nb = CreateDocument[contents, Visible -> False];
  SetOptions[nb,
    Saveable -> True,
    WindowSize -> {725, 750},
    WindowMargins -> {{0, Automatic}, {Automatic, 0}},
    PrivateNotebookOptions -> {"FileOutlineCache" -> False},
    TrackCellChangeTimes -> False,
    Visible -> True
  ];
  nb
];

BuildIndex[app_String] :=
 Module[
  {docDir, indexDir, index, spellIndexDir},

  docDir = FileNameJoin[{appDir[app<>"`"], "Documentation", "English"}];

  (* Create a new index *)
  indexDir = FileNameJoin[{docDir, "Index"}];
  CloseDocumentationIndex[indexDir];
  DeleteFile[FileNames["*", indexDir]];
  index = NewDocumentationNotebookIndexer[indexDir];
  
  (* Add all documentation notebooks to the index *)
  AddDocumentationDirectory[index, FileNameJoin[{docDir, "Guides"}]];
  AddDocumentationDirectory[index, FileNameJoin[{docDir, "ReferencePages"}]];
  AddDocumentationDirectory[index, FileNameJoin[{docDir, "Tutorials"}]];

  CloseDocumentationNotebookIndexer[index];

  (* Create SpellIndex *)
  spellIndexDir = FileNameJoin[{docDir, "SpellIndex"}];
  DeleteFile[FileNames["*", spellIndexDir]];
  CreateSpellIndex[indexDir, spellIndexDir];
  
  RestartPacletManager[];
];

UndocumentedSymbols[package_String] :=
  Select[Names[package<>"`*"], !StringQ[ToExpression[#<>"::usage"]] &];

UndocumentedSymbols[app_String, package_String] :=
  Select[Names[app<>"`"<>package<>"`*"], !StringQ[ToExpression[#<>"::usage"]] &];

DocumentedSymbols[package_String] :=
  Select[Names[package<>"`*"], StringQ[ToExpression[#<>"::usage"]] &];

DocumentedSymbols[app_String, package_String] :=
  Select[Names[app<>"`"<>package<>"`*"], StringQ[ToExpression[#<>"::usage"]] &];

SymbolDocumentedQ[s_String] :=
  StringQ[ToExpression[s<>"::usage"]];

GuideSection[package_String, detailed_List] :=
  {"Title" -> package,
   "DetailedFunctions" -> Apply[List, detailed, 1],
   "Functions" -> Complement[DocumentedSymbols[package], Map[First, detailed]]};

GuideSection[app_String, package_String, detailed_List] :=
  {"Title" -> package,
   "DetailedFunctions" -> Apply[List, detailed, 1],
   "Functions" -> Complement[DocumentedSymbols[app,package], Map[First, detailed]]};


End[];
EndPackage[];

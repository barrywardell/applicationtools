{
 "Application" -> "DocumentationBuilder",
 "Package" -> "DocumentationBuilder",
 "Title" -> "DocumentationBuilder Package",
 "Summary" -> 
   "Package for generating Mathematica documentation",
 "Description" -> "This page gives an overview of all the functions and symbols provided by DocumentationBuilder.",
 "Keywords" -> {"DocumentationBuilder", ToUpperCase["DocumentationBuilder"], ToLowerCase["DocumentationBuilder"]},
 "Label" -> "DocumentationBuilder Application",
 "Synonyms" -> {"DocumentationBuilder", ToUpperCase["DocumentationBuilder"], ToLowerCase["DocumentationBuilder"]},
 "URL" -> "http://bitbucket.org/barrywardell/documentationbuilder",
 "Packages" -> 
  {
   {"Title" -> "DocumentationBuilder",
    "DetailedFunctions" -> {
      {"BuildGuide", "build a guide page"},
      {"BuildSymbolReference", "build a symbol reference page"},
      {"BuildIndex", "build search index"},
      {"DocumentedSymbols", "list documented symbols in a package"},
      {"UndocumentedSymbols", "list undocumented symbols in a package"}
    },
    "Functions" -> {"MoreInformation", "TextComment"}
   }
  }
}


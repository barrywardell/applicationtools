Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"ParentDirectory", "[", 
    RowBox[{"NotebookDirectory", "[", "]"}], "]"}], "]"}], ";"}]], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"<<", "\"\<Source/GenerateDocumentation.m\>\""}]], "Input"],

Cell[CellGroupData[{

Cell[BoxData["\<\"Building symbol reference pages\"\>"], "Print"],

Cell[BoxData["\<\"Building guides\"\>"], "Print"],

Cell[BoxData["\<\"Indexing Documentation\"\>"], "Print"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{740, 756},
WindowMargins->{{Automatic, 114}, {Automatic, 12}},
PrivateNotebookOptions->{"FileOutlineCache"->False},
ShowSelection->True,
TrackCellChangeTimes->False,
FrontEndVersion->"8.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (October 5, \
2011)",
StyleDefinitions->"Default.nb"
]

